# About Databases

Most web applications need to keep track of data in order to run. Whether that be users, posts, likes, or whatever, it all has to be stored somewhere.
This is when we use databases.
One of the most common types of databases is a relational database which database systems such as MySQL, SQLite, or postgreSQL use.
A relational database can pretty easily be described like a spreadsheet or table.

We organize data into tables which are collections of similar things (such as users),
the columns represents attributes of the members of the collection (username, email, etc.),
and each row is an individaul entry in the collection (individual users)

For example:

| username | email              | first name | last name |
| -------- | ------------------ | ---------- | --------- |
| john89   | john89@gmail.com   | john       | Smith     |
| someone  | someone@gmail.com  | some       | one       |
| billy123 | billy@gmail.com    | billy      | billy     |

What makes relational databases so wonderful is the ability to query data from these tables (such as finding all users who have a first name starting with 'j').

## What is PostgreSQL?

PostgreSQL is an open source relational database management system that uses the SQL language to handle data.
PostgreSQL is what we will be using in this tutorial, but there are many different database management systems out there, each with different features and uses.

[How to create databases in postgreSQL](creatingDatabases.md)