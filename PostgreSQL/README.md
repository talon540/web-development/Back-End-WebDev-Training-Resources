# PostgreSQL Training Resources

This directory contains lessons that aim to establish a basic understanding of PostgreSQL.

While PostgreSQL will not be used directly in django, it is important to understand what goes on behind the scenes
to understand certain concepts.

## Table of Contents

1. [About databases](databases.md)
2. [Getting Started](creatingDatabases.md)
3. [Making Queries](queries.md)

## Installation Procedure
There is no need to install PostgreSQL on your pc, instead you should use an online IDE such as cloud9 which we will be using in this tutorial

### Practicing postgresql on cloud9
You can use any online IDE you want, but for this tutorial a cloud9 workspace has already been setup for you.
Also the cloud9 workspace contains the example database and table used in the following tutorials.

1. Go to [cloud9](https://c9.io/login) and login using the credentials given to you at the meeting
2. Select the workspace called 'postgresql-training-2018-2019' and open it
3. You should see a screen similar to this:

    ![The cloud9 workspace](../images/cloud9.PNG)

4. Once you are in the workspace you can maximize the terminal as nothing else is used in this tutorial
5. Now you can follow the rest of this tutorial
