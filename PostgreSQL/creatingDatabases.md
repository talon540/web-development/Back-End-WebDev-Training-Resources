# Creating databases and filling them with data

For this part of the tutorial make sure you have cloud9 open with a terminal available.

In order to start postgresql we have to run this command:
```bash
$ sudo service postgresql start
```

Once postgresql we can then access it by:
```bash
$ psql
```

from here we can type this command to create a database, which will contain tables
```bash
ubuntu=# CREATE DATABASE "example";
```

Now if we want to list all of the databases available we can run the \list command:
```bash
ubuntu=# \list
                             List of databases
   Name    |  Owner   | Encoding  | Collate | Ctype |   Access privileges   
-----------+----------+-----------+---------+-------+-----------------------
 example   | ubuntu   | SQL_ASCII | C       | C     | 
 postgres  | postgres | SQL_ASCII | C       | C     | 
 template0 | postgres | SQL_ASCII | C       | C     | =c/postgres          +
           |          |           |         |       | postgres=CTc/postgres
 template1 | postgres | SQL_ASCII | C       | C     | =c/postgres          +
           |          |           |         |       | postgres=CTc/postgres
 ubuntu    | ubuntu   | SQL_ASCII | C       | C     | 
(5 rows)
```

In order to do anything in the database we must first connect to it:
```bash
ubuntu=# \c example
You are now connected to database "example" as user "ubuntu".
```

From here we can now create tables and fill them with data. In order to create a table you must specify each column and its datatype.
In the example table that we are making here we have 3 columns. The first one is called 'number' and is an integer, then there is 'name' which has the datatype TEXT, a textfield with unlimited length. Finally there is email which has the datatype CHAR(50), meaning that it is a textfield but with a limit of a length of 50 characters.
```bash
example=# CREATE TABLE example_table(number INT, name TEXT, email CHAR(50));
```

A useful command to know is how to describe tables.
This command prints out each column of the table and what datatype is.
You may also notice the modifiers column, this would have things such as NOT NULL modifiers if we had decided to include them. If we did want to include them when we were creating our table we would have just added them after the datatype.
```bash
example=# \d example_table
    Table "public.example_table"
 Column |     Type      | Modifiers 
--------+---------------+-----------
 number | integer       | 
 name   | text          | 
 email  | character(50) | 
```

Now if we want to put data into this table we have to use the INSERT command.
With the insert command we have to supply the name of the table, the columns we want to put data in, and the values for each of those columns
```bash
example=# INSERT INTO example_table(number, name, email) VALUES (540, 'bob', 'bob@gmail.com');
```

And now we have managed to create a database, give it a table, and put some data into that data. Next we will learn how to interact with this data in [the next part of the tutorial](queries.md)