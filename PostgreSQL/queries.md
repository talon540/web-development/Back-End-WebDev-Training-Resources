# Using queries to retrieve data

In this part of the tutorial we will learn how to query the table we made in the previous table to get the data from it.

The most obvious thing we might want to do is get all the data in the table.
In order to do this we have to use the SELECT command.
The select command is structured in the order of the keyword SELECT then the columns you wish to select then the keyword FROM and then the table you want to select from.
In this case we used an asterisk instead of listing tables. In SQL an asterisk stands for ALL, so in this case it is selecting all of the columns.
There are also other optional parts of the command, but we will get to those later.
```bash
example=# SELECT * FROM example_table;
 number | name |                       email                        
--------+------+----------------------------------------------------
    540 | bob  | bob@gmail.com                                     
      1 | john | john@gmail.com                                    
   2000 | jim  | jim@gmail.com                                     
    300 | bill | bill@gmail.com                                    
    999 | alex | alex@gmail.com                                    
    666 | jack | jack@gmail.com                                    
(6 rows)
```

We could also select only some of the colums if we wanted to.
```bash
example=# SELECT name, email FROM example_table;
 name |                       email                        
------+----------------------------------------------------
 bob  | bob@gmail.com                                     
 john | john@gmail.com                                    
 jim  | jim@gmail.com                                     
 bill | bill@gmail.com                                    
 alex | alex@gmail.com                                    
 jack | jack@gmail.com                                    
(6 rows)
```

Now if we want to get a bit more specific with our queries we can use the WHERE keyword.
In order to use it we add it after the table name. After the where keyword we can specify a condition.
For example, this command only selects rows where the number in the number column is greater than 500.
```bash
example=# SELECT * FROM example_table WHERE number > 500;
 number | name |                       email                        
--------+------+----------------------------------------------------
    540 | bob  | bob@gmail.com                                     
   2000 | jim  | jim@gmail.com                                     
    999 | alex | alex@gmail.com                                    
    666 | jack | jack@gmail.com                                    
(4 rows)
```

There are also a lot more ways of querying data in specific ways. Such as querying from multiple tables at once, select the largest or smallest values, remove duplicates, search strings, and much more. Though for the purposes of this tutorial this is all we need to know, as we never directly use SQL in django, it is all done behind the scenes.
However, if you wish to learn more about SQL the best way is [to practice](https://pgexercises.com/)