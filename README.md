# Back-end Web Development Training Resources

## Overview
The purpose of this repository is to provide training resources for backend develepoment of the talon 540 service website for use in the FIRST Robotics Competition.
These resources are meant to be used as a reference for learning each of the aspects and steps of developing the backend of the talon 540 service website.
Not all of the pages here are required to be read in order to begin development, instead some sections are meant to be read as they are needed.

## Resources Required
- A Windows or a GNU/Linux machine
- Internet Access

## Lessons
- [Linux](terminal/README.md)
- [Python](python/README.md)
- [PostgreSQL](PostgreSQL/README.md)
- [Django](django/README.md)
- [Git](git/README.md)
- [Deploying to Production](production/README.md)

## General Documentation
- Linux Terminal Commands: https://linux.die.net/man/
- Python: https://docs.python.org/3/
- PostgreSQL: https://www.postgresql.org/docs/manuals/
- Django documentation: https://docs.djangoproject.com/en/2.0/
- Git: https://git-scm.com/doc