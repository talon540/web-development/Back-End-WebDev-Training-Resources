#This program creates several different types of variables and prints them out

#You may notice that python does not need a main method to run a program
#Instead it reads through the program line by line

#In python we do not need to state what kind of variable we are creating.
#Instead, python figures that out automatically for us

#an example of an integer in python
anInt = 999

#an example of a double in python
aFloat = 9.9

#an example of a string in python
aString = 'hi'

#an example of a boolean in python
#Booleans in python start with a capital letter
aBool = True

#in order to print something out we use the print() function
print(anInt)

#in order to print a string with a nonstring variable you need to parse the variable to a string using the str() function
print('the float is ' + str(aFloat) + '!')

#if the variable is already a string it does not need to be parsed
print('the string is ' + aString)

print('the bool is ' + str(aBool))

#printing with string formatting

#String formatting is another way of printing variables in strings using the print function
#The %s stands for a variable which is automatically converted to a string

#for example if we wanted to print an integer we would do this:
print('This is printing an int using string fomratting: %s' % anInt)
#and this is how we would print multiple variables:
print('This is printing multiple values: int - %s, double - %s, string - %s, and boolean - %s' % (anInt, aFloat, aString, aBool))
#There are several different special characters that can be used, but %s can work for anything with a string representation
#A few are:
#   %s - a string or anything with a string representation
#   %d - integers
#   %f - floats
#   %.<a number of digits>f - flot rounded to a certain number of digits after the decimal
#   %x/%X - integers in lower or uppercase hex representation
#these can be used to convert variables in different ways, such as rounding a float

#For the most part though it does not matter whether you use string formatting or the other method, it just comes down to personal preference