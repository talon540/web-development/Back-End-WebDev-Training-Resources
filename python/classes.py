#in python there are also classes and methods

#a class in python contains instance variables at the top and then the methods that belong to that class after that
class ExampleClass:
    i = 10

    #a method is simply a function that belongs to a class
    #all methods are functions but not all functions are methods
    #the first parameter of a method is self which is the instance the method belongs to
    #this can be used to access the variables that belong to that instance of the class
    def hello(self):
        return 'hello there'

#We can now create an instance of the example class and then interact with it
a = ExampleClass()
print(a.i)
#prints 10
print(a.hello())
#prints hello there

#if we want to have a constructor for a class then we have to create a method with the name '__init__'
#this method is automatically called when an instance of the class is made
class Constructor:
    #in order to include paramters in a method we simply add them after the self paramater
    def __init__(self, a, b, c):
        self.a = a
        self. b = b
        self.c = c

#then we pass the arguments for the constructor here
foo = Constructor(1, 2, 3)
#and now all of the variables have been assigned accordingly
print(foo.a)
#prints 1
print(foo.b)
#prints 2
print(foo.c)
#prints 3

#Here is an example class that uses several different methods
class MathMethods:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def add(self):
        return self.a + self.b

    def subtract(self):
        return self.a - self.b

    def multiply(self):
        return self.a * self.b

    def divide(self):
        return self.a / self.b

math = MathMethods(10, 7)
print(math.add())
print(math.subtract())
print(math.multiply())
print(math.divide())
