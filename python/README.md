# Python Training Resources

This directory contains example python code for the back-end web development training.

Python is required for use in django.

## Table of Contents

This is a list of the example python programs that teach various parts of python

1. [Assigning variables and printing them](variables.py)
2. [Comparing values using if statements](statements.py)
3. [Loops in python](loops.py)
4. [Creating functions](functions.py)
5. [Using clases and methods](classes.py)

## Installation procedure
For the purposes of this tutorial it is possible to use an online IDE such as [repl.it](https://repl.it/languages/python3),
however, it is still recommended to install and use python on your computer because of the ease of access and use

### Windows and Mac

The process for windows and mac should be mostly similar the only differences should be in how the installer looks, but the process should be the same.

1. Go to [the python download page](https://www.python.org/downloads/) and download the latest official version of python 3
2. Run the Installer
3. Simply click the Install now button

    ![The python installer.](../images/pythonInstaller.PNG)

### Linux

Python 3 is already installed for you so there is no need to install anything.

## Basic Usage
In order to write python code open the python idle application, go to the file tab, and click new file (or use the shortcut, Ctrl+N).
Your code can be run by pressing f5 or going to the run tab and selecting 'run module'

The idle application can be used to run any command in python that you could normally run in an actual program, 
for example we can use it to print hello world:

```python
>>> print('Hello World')
Hello World
>>>
```

You are also able to do basic math in the idle

```python
>>> 1+1
2
>>> 6*3
18
>>> 8/2
4.0
>>> 5-1
4
>>> 
```

It is also possible to do much more such as assigning values to variables, or just doing anything you can in actual python applications.
