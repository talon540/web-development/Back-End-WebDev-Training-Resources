#in python we can create functions (note: these are different from methods which we will talk about later)
#functions are bits of code that we can call at any time after they are declared
#functions can also have parameters

#functioons are defined by using the keyword def
def greet():
    print('Hello there person')

#We can then call the function like this
greet()

#this is an example of a function with parameters
def sum(a, b):
    return a + b

#since we return the sum of the two numbers the sum(1, 2) will be replaced by 3 which is what we will print
print(sum(1, 2))

#of course it is also possible to include if statements or loops in functions
def example(a):
    if a > 0:
        return 1
    elif a < 0:
        return -1
    else:
        return 0

print(example(99))