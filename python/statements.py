#assigns a number using the input() function then checks it using if statements

#In python we can get input from the user using the input() function, which gets the input as a string
aNumber = int(input('what is your favorite number'))

#in python if statements do not use { or } do determine where they start and end. 
#Instead, the if statement starts on the line with if and :, and anything indented after that is within the if statement.

#in python there are still operators such as ==, >, or < like there were in java.
if aNumber == 10:
    print('the number is 10')

#python uses elif instead of else if
elif aNumber > 0:
    print('the number is positive')
    
elif aNumber < 0:
    print('the number is negative')

else:
    print('the number is 0')

if aNumber > 10 and aNumber < 20:
    print('the number is between 10 and 20')
    
elif aNumber <= -1000 or aNumber >= 1000:
    print('the number is 1000 or more away from 0')
    
elif (aNumber <= 10 and aNumber >= -10) or aNumber == 22:
    print('the number is either between 10 and negative 10 or is 22')

else:
    print('the number didnt match any of the other conditions')

