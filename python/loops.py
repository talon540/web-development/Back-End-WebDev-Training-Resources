#this program shows several examples of loops in python

anInt = 20

#while loops are fairly similar to if statements in terms of how they are structured, and they will run as long as the condition is true
while anInt > 0:
    print(anInt)
    anInt -= 1

#for loops are a bit different however
#and are also a bit different from how for loops work in java
#for loops run through every value in a sequence
#as an example this is a for loop that runs through an array
anArray = [1, 2, 3]
for val in anArray:
    print(val)

#if you want to run a for loop for a certain range of numbers you use the range() function
#the range function starts at and includes the first number and runs up to but not including the second number
#this example runs from 0-19
for i in range(0, 20):
    print(i)

#this for loop would be the same as this in java:
#for (int i = 0; i < 20; i++) {
#   System.out.println(i);
#}