# Production Training Resources

This directory contains resources for deploying your django code to a production server so that 
you can run your website so that it is publicly viewable

## Table of Contents
1. [aws](aws.md)
2. [setting up server](server.md)
3. [getting django ready for production](django.md)
4. [gunicorn](gunicorn.md)
5. [nginx](nginx.md)