# Basics of git

This is going to cover the process of creating a local repository on your own machine, commiting changes,
as well as the general ideas behind using git.

## Creating a repo

Git works through the use of "repos" or repositories. This is basically the project folder that you put
all of the source code, or whatever you want to track, inside of. Git will automatically detect any files
in the repo and any git commands you run will be based on what repo you are in.

To create a repo we can either create a new local one on our current machine, or copy an existing one
from somewhere else.

## Init from existing folder
To create a new repo, make an empty folder for your project and run:
```
git init
```

This command will have created .git folder within the current one. This folder holds all the internals for
your repo, though you won't ever have to go inside it.

Note that the folder doesn't necessarily have to be empty, if there are already files you will have to add
the files using
```
git add
```

## Clone from elsewhere
Cloning a directory is very similar.

For example, to clone our website repo you just have to run:
```
git clone https://gitlab.com/talon540/web-development/Talon-540-Service-Website-2019.git
```

This creates a new folder called "Talon-540-Service-Website-2019" that now acts as a local repo.

