# Git Training Resources

This directory contains information on using git and the way that we use it for the website.

Git is a version-control system that allows us to easily work on the website from many different devices at once.

## Table of Contents
1. [What is a version-control system?](version.md)
2. [The basics](basics.md)
3. [Branches, remote repos, and more](more.md)
4. [Our branching model](model.md)

## Installing Git

You're going to need to install git and create a gitlab account.

Git has instructions for installing it [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). Note that there
are GUIs available for git, but we will only be talking about using git from the command line.