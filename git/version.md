# What is a Version-Control System

Before we can learn how to use git we must first talk about what version control and git actually are.

## What is version control?

A version control system (VCS) keeps track of all the different versions of files for a project.
It may not be clear right away why a VCS is so important, but as you use one more you will begin
to understand that it is vital to use one for any project where multiple people are working together.

## First version control systems

The original version control systems (VCS) took the simple approach of simply storing previous versions
of the source code being tracked in a seperate folder. However, instead of simply storing the entire files,
they only stored the changes made. This kept the size down while allowing many different versions to be stored.

<img src="https://git-scm.com/book/en/v2/images/local.png" width="400" height="342">

Image Source: https://git-scm.com/book/en/v2/images/local.png

## Sever based VCSs

The limitation with the previous system was that it did not allow multiple people to work on the same project
unless they were using the same computer. The solution to this was to host the database on a dedicated server,
people working on the source code could then retrieve the files from the server, make their changes, and send
the files back to the server. You could even have multiple people working on files at once on seperate machines,
the changes would simply be merged once the files were sent to the server.

<img src="https://git-scm.com/book/en/v2/images/centralized.png" width="400" height="278">

Image Source: https://git-scm.com/book/en/v2/images/centralized.png

## Modern VCSs

Modern VCS use a a combination of the previous two types of VCS. There is still a dedicated server holding the
source code, but now instead of receiving only the current files, you receive a copy of the entire database.
This allows for many different backups incase the server goes down, as well as allowing for more collaboration between
people.

<img src="https://git-scm.com/book/en/v2/images/distributed.png" width="334" height="400">

Image Source: https://git-scm.com/book/en/v2/images/distributed.png

## What about git?

Git is different in that it uses snapshots rather than differences. Basically, any time you "commit" or save your project
git will take a picture of what you have. To save space, if a file hasn't changed since the last commit git will just
store a link to the last snapshot of that file instead of creating a new one.

A file can be in three states:
1. modified - the file has been changed since the last commit
2. staged - you have marked this file to be saved in the next commit
3. commited - the file is up to date in your local database

## Next

Now let's start using [git](basics.md)