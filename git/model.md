# Our Branching Model

For the website we use a certain branching model.

## Main branches

This model has two main branches that will always exist on the remote repo.

- master - this contains the production code, the current version on this
  must be ready to deploy to the production server at any time.
- develop - this contains the pre-production code. Any features being worked on
  will be merged into this branch. This branch is then merged into the master
  branch once it is ready for the next deployment to production.

## Temporary branches

These are temporary branches that will typically only exist on the local repos
of developers.

- feature - feature branches are for well, working on features. This branch will
  exist for as long as the feature is in development. Once the feature is finished,
  it is merged into the develop branch.
- release - a branch for making last minute changes inbetween develop and master.
  we make these branches so that we can continue to work on the develop branch while
  also preparing for the next release.
- hotfix - these branches are made for when a change needs to be quickly made to the
  master branch, usually for bugs that result in features being unusable. Hotfix branches
  are created from the master branch and merge into both the development branch and the
  master branch once they are done.