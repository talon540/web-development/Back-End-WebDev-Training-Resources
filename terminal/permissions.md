# Permissions

Permissions is the system that Linux uses to determine what users can edit, open, or run a file. There are three permissions for each of three user types.

## User Types / Owners

Every file and directory has permissions for each of three categories:

1.  User - the user who created the file. 

2.  Group - a user group contains multiple members and applies the same permissions to all of its members. Instead of manually assigning the same permissons to each user, you assign the permissions to the user group and then add the file and users to the group.

3.  Other - everyone else.

Each of these three categories has three permissions.

## Permissions

Just like with categories, there are three permissions:

1.  Read - On a file, gives the user authority to open a file and look at its contents. On a directory, gives the user authority to list the files and directories inside it.

2.  Write - On a file, gives the user authority to edit a file's contents. On a directory, gives the user authority to add, remove, move, and rename files within the directory. If you have write permission on a file but not the directory that contains it, you will be able to edit the contents but not rename, move, or delete the file.

3.  Execute - In Linux, you cannot run a program unless you have the execute permission for it. If you have this permission on a file, you can run it.

Each of the three user categories can have any, all, or none of these permissions set. 

## Viewing Permissions

To see what permissions are set on a file or directory, you can use `ls -l`, which will display the permissions before every file and directory that is shown. The format is concise, but can be confusing at first.

Example:<br/>
`$ ls -l /etc`<br/>
`total 492`<br />
`-rw-r--r-- 1 root root  4468 Nov 19  2009 DIR_COLORS`<br />
`-rw-r--r-- 1 root root    10 Jun 30 03:29 adjtime`<br />
`drwxr-xr-x 4 root root  4096 Jun 30 03:44 apache2`<br />
`drwxr-xr-x 2 root root  4096 Nov 19  2009 bash`<br />
`drwxr-xr-x 3 root root  4096 Nov 19  2009 ca-certificates`<br />
`-rw-r--r-- 1 root root  5955 Nov 19  2009 ca-certificates.conf`<br />
`drwxr-xr-x 2 root root  4096 Jul  5 20:37 conf.d`<br />
`drwxr-xr-x 2 root root  4096 Dec  3  2009 cron.d`<br />
`drwxr-x--- 2 root root  4096 Dec  3  2009 cron.daily`<br />
`-rw-r--r-- 1 root root   220 Dec  3  2009 cron.deny`<br />
`drwxr-x--- 2 root root  4096 Dec  3  2009 cron.hourly`<br />
`drwxr-x--- 2 root root  4096 Dec  3  2009 cron.monthly`<br />
`drwxr-x--- 2 root root  4096 Dec  3  2009 cron.weekly`<br />
`-rw-r--r-- 1 root root   611 Dec  3  2009 crontab`<br />
`...`

The first column of details looks like `-rw-r--r--`. This phrase contains all the permissions for the file or directory it precedes. 

The first character will be either a '-', indicating a file, or a 'd', indicating a directory. More rarely, it may be an 'l', indicating a symlink, which is a pointer to another location in the file system (think of a Windows file shortcut).

The nine characters following it are divided into three sets of three characters. 'r' indicates read permissions, 'w' indicates write permissions, 'x' indicates execute permissions, and '-' indicates the absence of a permission. These letters will always be in the same order - "rwx" - so a '-' just replaces the letter that would be there normally. The first set of three shows the User permissions, the second shows Group permissions, and the third shows Other permissions.

The number following this isn't relevent to permissions, but the next two columns are also important. The first name indicates the file owner ('root' in the examples above), while the second indicates the file group. In the example above, all of the visible files were created by the `root` user and belong to the `root` group.

## Changing Permissions 

The command `chmod [abbreviation] [filename]` is used to edit file and directory permissions. The abbreviation is composed of at least three of the following parts:

*  At least one user type ('u' for user, 'g' for group, 'o' for other, or a' for all of u, g, and o)
*  An operator ('+' to add a permission, '-' to remove a permission, or '=' to set the exact set of file permissions)
*  At least one permission ('r' for read, 'w' for write, or 'x' for execute)

An example chmod command might look like the below:

`chmod a+rw file1`

This command sets read and write permissions for all user types on a file called `file1` in the current directory. Note that this assumes that all user types already have these permissions for the current directory; you'll have to set those permissions separately if they don't.

In the next lesson, you will learn about [Piping commands](https://gitlab.com/talon540/web-development/Back-End-WebDev-Training-Resources/master/piping.md).