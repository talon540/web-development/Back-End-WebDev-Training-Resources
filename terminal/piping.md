# Piping
Piping is a technique to combine multiple commands into one by using the output result of one command as the input value for another. This allows commands to operate continuously, without requiring you to manually input the results of the first command to the second.

## Syntax
Piping is done by placing a pipe symbol `|` between two commands, like so:

`command_1 | command_2 | command_3 | .... | command_N`

For example, `ls -l | more` will feed the output of `ls 'l` (the list of all files and directories in the current directory) into the input of `more`, resulting in the `ls` results being displayed in pages rather than all at once.

## Grep
Piping is especially powerful in combination with filter commands. The most-used filter command is `grep [search_term]`, which searches its input for a specific string.

Example: `cat sample | grep Apple` searches the contents of the file `sample.txt` (retrieved by `cat sample`) for the string `Apple`.

The next lesson will teach you about 