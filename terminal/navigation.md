# Basic Navigation

When logging onto a linux server there isn't actually any sort of desktop environment to use like you might be used to,
instead you will have to use a terminal to input commands in order to interact with the system.

## What am I looking at?

What you see may vary slightly depending on the system, as it can be customized, however the terminal should generally look something like this:

```
bob@talon:~$
```

Here is the breakdown for this example:
- bob: the current active user
- talon: the hostname of the system
- ~: the current active directory. In this case ~ is a shortcut for the user's home directory (a directory is what linux calls folders), so in this case /home/bob
- $: The prompt symbol that indicates that it is waiting for user input

## Where am I?

Whenever you are using the terminal you will have a working directory. This is where you are currently located within the file system and will be
where relative file paths start from. So if you're current working directory is your home directory and you have a file called example.txt then
the relative path would simply be example.txt instead of the full path of /home/bob/example.txt

Usually your working directory will appear to the left such as in the example in the what am I looking at section, however there is also a command to view
your current working directory. The command is "pwd"

```
bob@talon:~$ pwd
/home/bob
```

## How do I change my working directory?

Usually when working in the terminal you want to be able to navigate through the file system so that you don't have to always type the full name of every file.
There are two commands that you will use the most when it comes to navigating, `cd` and `ls`.

### The Change Directory Command

cd, which stands for change directory, is the command you will use to change your current working directory. Typically commands are more complicated than the `pwd` command
from before and will require additional options and parameters. Commands in linux follow the structure of `command <options> <arguments>`. Here you have the command itself,
any options to affect the command in various ways, and then the actual arguments of the command which may or may not be required. It is possible to have multiple
options and arguments at the same time. We will see some examples of this later.

For `cd` however, you won't have to worry about any of the options and there is only one argument needed. This argument is the path to the directory you wish to move to.
For example:

```
bob@talon:~$ cd dir1/dir2
bob@talon:~/dir1/dir2$
```

In this example the user is in their home directory with a directory with a directory called dir1 inside it and a directory called dir2 inside that. The user then navigates
to dir2.

`cd` does also work with absolute paths so in this example the user could've instead used the argument /home/bob/dir1/dir2, or they also could've used the shortcut for
the home directory and done ~/dir1/dir2.

### List Directory Contents

The `ls` command is used to list the contents within any directory. The `ls` command has one argument that it needs, which is the path to the directory you wish to
see the contents of. However, it is possible to use `ls` without any arguments, the command will then automatically use your current working directory as the target
directory. For example, to list everything within the your home directory:

```
bob@talon:~$ ls
file1.txt file2.txt file3.txt dir1
```

In this example the first three entries are files while the last entry is a directory.

Unlike the `cd` command, `ls` has many optional arguments (we'll go over how to view all of these
options in the man pages tutorial). The option you use the most will probably by the -a option which forces ls to show hidden files (files or directories that start with
a .). Personally I also like to use the -l opion which will format the results from `ls` into a list format. In linux we can use these options by adding a dash after ls
and listing all the letters that stand for the options that we want to use.

```
bob@talon:~$ ls
*dir*  file1.txt  file2.txt
bob@talon:~$ ls -la
total 16
drwxr-xr-x  4 bob bob 4096 Aug 13 14:55 .
drwx------ 19 bob bob 4096 Aug 13 14:54 ..
drwxr-xr-x  2 bob bob 4096 Aug 13 14:55 .hidden-dir
drwxr-xr-x  2 bob bob 4096 Aug 13 14:55 dir
-rw-r--r--  1 bob bob    0 Aug 13 14:55 file1.txt
-rw-r--r--  1 bob bob    0 Aug 13 14:55 file2.txt
```

Don't worry too much about the first four columns, we'll talk more about those in the permissions tutorial. After the fourth column, we then have the size of the file/directory
in bytes, the month the file/directory was last saved, the day, the time, and then the name of the directory or the file. You may be wondering about the first two rows
which have directories called "." and "..", these aren't directories within your current directory. Instead, "." is a shortcut for the working directory and ".." is a shortcut
for the directory above the working directory. So in this example "." is ~/example and ".." is the home directory. You can use these shortcuts as arguments for commands
such as `cd` or `ls`.

---

That covers the basics of navigation within the terminal on linux, next you will learn about interacting with files and directories in the [next tutorial](directories.md)
