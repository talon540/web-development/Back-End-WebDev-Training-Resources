# Files and Directories

Files and directories are the most basic level of any computer's filesystem, including the linux server. 
Everything on the computer is stored in files and organized through directories.
Every time you interact with the server, you'll need to interact with the filesystem.

## Files

Files are the informational elements of the filesystem. Files include text documents, images, sound files, programs, and every other type of data stored on the computer. 

## Touch
The `touch` command allows you to create new empty files. Its syntax is:

`touch [option] file_name(s)`

If no options are provided, new files are created for every name provided. For example, `touch file1 file2 file3` would create files named file1, file2, and file3 at the current directory location. Its options allow file timestamps to be modified, which we rarely use.

## Directories

Directories are the organizational elements of the filesystem. Directories can contain files as well as other directories. Every file and every directory (except `/` - see The Linux File Hierarchy below) is contained within another directory.

## Mkdir

The `mkdir` command allows you to create new empty directories. Its syntax is:

`mkdir [option] directory_name(s)`

New directories are created for every name provided. For example, `mkdir dir_1 dir_2 dir_3` would create directories named dir_1, dir_2, and dir_3 at the current directory location. 

`mkdir`'s options allow you to control file permissions, which will be covered in a few lessons. For more info, see [Permissions](https://gitlab.com/talon540/web-development/Back-End-WebDev-Training-Resources/master/permissions.md).

## The Linux File Hierarchy 

All files on the server are contained somewhere within the file hierarchy. Linux has a number of directories already created and used for specific purposes.

`/`, also called the root directory, is the highest level of the file hierarchy. All directories are contained here. Only the root user has write permissions here. This directory is NOT the same as the root user home directory, which is `/root/`.

`/bin/` contains binary command executables usable by all system users through the console.

`/boot/` contains files used to load on startup, including Kernel initrd, vmlinux, and grub files.

`/dev/` contains device files, including USB and other devices attached to the system.

`/etc/` contains system config files required by all software, as well as shell scripts for starting and stopping individual programs.

`/home/` contains the home directory of every user on the system, which hold personal files and settings.

`/lib/` contains libraries used by binary executables. 

`/media/` is a temporary directory that holds mount points for removable devices: CD-ROMs, floppy disks, etc.

`/mnt/` is a temporary directory that holds mounted filesystems: CD-ROMs, flashdrives, etc.

`/opt/` contains optional application add-ons for individual programs, provided by the vendor.

`/sbin/` contains system binary executables, usually used for network administration and maintenance. 

`/srv/` contains server and network service data, including web scripts and data, FTP server data, and version control repositories.

`/tmp/` contains temporary files. It often has strict file size restrictions, and all files in it are deleted when the system reboots.

`/usr/` contains multi-user applications, including binary files (`/usr/bin/` and `/usr/sbin`), libraries (`/usr/lib/`), programs installed from source (`/usr/local/`), and Linux kernel source code and documentation (`/usr/src/`).

`/proc/` contains automatically generated process and kernal info files. 



## Hidden files and directories

Some files and directories, mostly important system files or configuration files, are hidden and do not appear when using the `ls` command. Files are hidden by prefixing their filename with a period, like this:

`.hidden.txt`

If you can't find a file using `ls`, it may be hidden. Using `ls` with the `-a` argument (`ls -a`) will show all files and directories, including hidden ones.

In the next lesson, you will learn about [Manual pages](https://gitlab.com/talon540/web-development/Back-End-WebDev-Training-Resources/blob/master/terminal/man.md).