# Manual Pages

Linux offers many different commands with many of them being incredibly useful and powerful. Of course with so many commands
it becocmes much more difficult to remember them and how to use them. Fortunately, linux has something called manual pages which
we can use to solve this problem.

## What are they?

The manual pages are a set of pages that contain every command you have available as well as how to use them. This includes any arguments 
or options used by the command. In order to actually use the pages you can use this command:

```
man <command to search for>
```

Once the command is used you will find that instead of seeing the command line, you now see a page with information about the command 
you searched for. You can scrolls up and down by using the arrow keys. It will look something like this:

```
bob@talon:~$ man ls
Name
    ls - list directory contents
 
Synopsis
    ls [option] ... [file] ...
 
Description
    List information about the FILEs (the current directory by default). Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.
 
    Mandatory arguments to long options are mandatory for short options too.
 
    -a, --all
        do not ignore entries starting with .
 
    -A, --almost-all
        do not list implied . and ..
        
...
```

As you can see the man page includes information such as the name of the command, a synonpsis which shows a quick example of the command, 
a description of what it does, as well as any options that the command may have.

You can then press q to exit out of the pages.

## searching

Of course you won't always remember the actual name of the command. For times like this, the man command has a search function that you 
can use. It is simply:

```
man -k <search term>
```

If you want to search within a man page you can press'/' then type what you want to search. Press enter to begin the search and use 'n' 
to cycle through the results if there is more than one matching result.

---

Those are the basics of the man pages. They are a powerful tool that you should keep in mind whenever you are working on a linux machine. 
The next section is about [permissions](permissions.md), where you will learn about users in linux.