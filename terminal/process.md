# Process Management 
Processes are the way linux runs every runnable program, including executables and commands. When a program is run, linux creates an instance holding all the resources used by the program. 

## Types of processes
There are three (technically two) types of processes in Linux:

*  Foreground/Interactive Processes: These processes can only be started by a user and require user input to run. All applications (such as .exe files) and terminal commands are foreground processes.
*  Background/Non-Interactive/Automatic Processes: These processes are started automatically as part of the system and don't take input.
*  Daemons: These are a special type of background process that starts when the system does and never dies. However, unlike normal background processes, they are able to be customized by the user through scripting.

## Process-related commands
There are two commands to look up processes, `ps` and `top`. `ps` displays a list of all the processes that are running when it is run, while `top` shows a real-time list of all running processes.

To kill a process, use `kill <process PID>`.

## Process IDs

Each process has a unique process ID (PID). If a process creates a new process while it runs, the new process (called the **child process**) has its own PID but also has its parent's ID (the PPID). 

Any process's PID can be found using the `pidof <process>` command. `init` always has a PID of 1 and functions as the parent for all processes on the system.

## Background and Foreground
Once you run a program, you can move its process between the background and the foreground. In the foreground, it runs as normal. In the background, it continues to run, but doesn't read user input. 

To start a process in the background, add `&` after the command: for example, `./myscript.py &` starts `myscript.py` in the background.

To move a background process to the foreground, use the command `fg <process PID>`.

##Process states
Depending on what happens during the process's execution, its state may change. The most common case of this is quitting a program, where the process switches from its normal running state to its shutting-down state.

There are four process states:
*  Running: The process is either currently being run or is waiting to be assigned to a CPU to be run.
*  Waiting: The process is waiting for a specific event or for memory, etc. to be freed up. Some waiting states (usually event-realted) are **interruptable**, meaning they can be interrupted by the system, while others (mostly dependent on hardware) are **uninterruptable** processes and cannot be changed without a hardware change.
*  Stopped: The process has been stopped by a signal, either because it's being shut down or for debugging purposes.
*  Defunct or "Zombie": The process has been killed, but still has an entry in the list of processes.

Congratulations, you've reached the end of the linux lessons!