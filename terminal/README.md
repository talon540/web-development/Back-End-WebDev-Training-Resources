# Linux Training Resources

This directory contains resources for learning and working with linux.

Linux is used as the OS of the server the website is run on, and will be the OS you will work in while developing any Django projects.

## Table of Contents
1. [Basic Navigation](navigation.md)
2. [Files and Directories](directories.md)
3. [Manual Pages](man.md)
4. [Permissions](permissions.md)
5. [Piping commands](piping.md)
6. [Process management](process.md)

## Using Linux

When working on the website you will most likely work in a free linux environment provided by one of various services. We'll go more depth into this in the Django training resources.
For the purposes of these tutorials you can instead use a website such as http://www.tutorialspoint.com/unix_terminal_online.php which allows you to practice
the commands from these tutorials within your browser.