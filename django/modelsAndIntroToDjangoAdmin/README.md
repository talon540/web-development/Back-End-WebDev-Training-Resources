# Models and an Intro to the Admin Interface
Now that you understand the basics of how views and urls work, it is important to learn about models.
Think of models as tables from postgresql, but all the statements are done by django for you.
This means that you simply need to tell it what the fields you want are and it will handle the rest.

The obvious thing we are going to need for our forums app is model for the posts.
For that we can do something like this:
```python
from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    post_date = models.DateTimeField('date posted')
    views = models.IntegerField(default=0)
    
    def __str__(self):
        return self.title
    
class Reply(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    reply_date = models.DateTimeField('date replied')
    
    def __str__(self):
        return self.text
```
In this example the post class is a model that we can use to store data in. This extends models.Model from django so that it can be recognized as a model.
In the class itself we have several different fields.
The name of each field is the field's name in machine-friendly format and is what you will use in your code to reference it. You can also include a human readable name as an optional first paramater to any of the fields.
Each field is set to an instance of a field class such as charfield.
These may have required arguments, like charfield which requries a max length.
Something to take not of though is the foreignkey field in the reply model. This establishes a many-to-one relationship between reply and post. Basically, this just means that each post will have a relationship with many different replies, and each reply will have a relationship with only one post.
You also might notice the \_\_str\_\_ methods in each of the models.
These are to string methods and will be called if you ever try to convert an object in the model to a string.

Now before we can have django setup the databases we need to tell it that we want to use the forums app. In order to this go to settings.py and look for this:
```python
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```
And add the app to the top like this:
```python
INSTALLED_APPS = [
    'forums',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

Django is now able to create the migrations for your database. Migrations simply store the changes that you have made to the models in a way that django can use.
```shell
python manage.py makemigrations
```

And now we are able to use the migrate command which will run the migrations and manage the database automatically, but if you want to understand more about what your migrations are actually doing then you can run the sqlmigrate command which will return the sql commands that the migrations will run.
```shell
python manage.py sqlmigrate forums 0001
```

Finally, run the migrate command.
```shell
python manage.py migrate
```

And now you have a working database.

## Using the django interface
Now that you have a working database you probably want to actually test it out. However, adding functionality to your views in order to test your database would take time, and we want to test it right now.
Fortunately django provides an admin interface that we can use to interact with any models that we have.

First we have to create a superuser which we will login to the admin interface with. Simply enter the username and password you want to use as it prompts you for them (it will ask you for an email, but you can just leave it blank)
```shell
python manage.py createsuperuser
```

Now go to example.com/admin/ and login.                                              
    ![admin login](../images/adminlogin.PNG)

And you should now see this screen:                                               
    ![admin interface](../images/adminblank.PNG)
    
Though you might notice that we cant actually see our models anywhere. This is because we need to register them with the admin interface. Though luckily django makes this really easy for us to do.
Simply go to admin.py in your app and add this.
```python
from django.contrib import admin

from .models import Post, Reply

admin.site.register(Post)
admin.site.register(Reply)
```
It is that simple. And now if we go back to the admin interface we should see our models.
    ![admin models](../images/adminmodels.PNG)

Feel free to explore the interface a bit and try adding a few entries to each model.

Once you feel comftorable using the admin interface lets move onto actually showing the data from your models in your views.

## Using your models with your views

Now to actually interact with our models in our views we have to use the model manager.
We can access it by using ModelName.objects. For example, Post.objects
The manager has several different functinos we can use, these include but are not limited to:
* all() - gets all objects in the model
* filter() - think if it as a sql where clause, you can use it to get a list of objects based on the paramters you give it
  * Ex: Post.objects.filter(views=1)
  * You can also use field lookups for more types of queries. You indicate a field lookup with a double underscore.
    * Post.objects.filter(title__startswith='test')
    * Some other examples include lte (less than or equal), gte (greater than or equal), exact, contains, and more.
* exclude() - filter but the opposite
* get() - filter but only returns one value and will raise an error if there are either 0 values or more than 1.
* and many more

So now you can update your views.py file to this:
```python
from django.http import HttpResponse

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # Puts all of the objects in the recent post list into a string and seperates them with commas. Ex: post1, post2, post3
    output = ', '.join([p.title for p in recent_post_list])
    # Adds a space after the recent posts
    output = output + '<br>'
    # Does the same as the recent posts but with the popular posts
    output = output + ', '.join([p.title for p in popular_post_list])
    #Finally returns the string as a httpresponse
    return HttpResponse(output)


# Shows a psot based on the url
def detail(request, post_id):
    post = Post.objects.get(pk=post_id)
    return HttpResponse(post)

# Browse all the posts with 5 posts to a page
def browse(request, page_num=1):
    # Orders the posts by their post date and then only gets the ones belonging to the current page
    post_list = Post.objects.order_by('-post_date')[(page_num-1)*5:page_num*5]
    # Puts the posts into a comma seperated string
    output = ', '.join([p.title for p in post_list])
    return HttpResponse(output)
```

And now if you look at your website each view should be working as the comments state.

Now go ahead and take a break and go complete assignment 2 in the drive, it should be cealled django basics.

You are now ready to move onto [using templates and namespacing your urls](../templatesAndNamespacingUrls/README.md)