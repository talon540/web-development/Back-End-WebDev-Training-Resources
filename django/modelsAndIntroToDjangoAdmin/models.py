from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    post_date = models.DateTimeField('date posted')
    views = models.IntegerField(default=0)

    def __str__(self):
        return self.title

class Reply(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    text = models.TextField()
    reply_date = models.DateTimeField('date replied')

    def __str__(self):
        return self.text
