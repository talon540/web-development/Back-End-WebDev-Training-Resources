from django.http import HttpResponse

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # Puts all of the objects in the recent post list into a string and seperates them with commas. Ex: post1, post2, post3
    output = ', '.join([p.title for p in recent_post_list])
    # Adds a space after the recent posts
    output = output + '<br>'
    # Does the same as the recent posts but with the popular posts
    output = output + ', '.join([p.title for p in popular_post_list])
    #Finally returns the string as a httpresponse
    return HttpResponse(output)


# Shows a psot based on the url
def detail(request, post_id):\
    post = Post.objects.get(pk=post_id)
    return HttpResponse(post)

# Browse all the posts with 5 posts to a page
def browse(request, page_num=1):
    # Orders the posts by their post date and then only gets the ones belonging to the current page
    post_list = Post.objects.order_by('-post_date')[(page_num-1)*5:page_num*5]
    # Puts the posts into a comma seperated string
    output = ', '.join([p.title for p in post_list])
    return HttpResponse(output)
