from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from datetime import datetime

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # The context is what will be passed to the template. The strings on the left are the names of the variables in the template, and the variables on the right are what they are asigning the template variables to.
    context = {
        'recent_post_list': recent_post_list,
        'popular_post_list': popular_post_list,
    }
    # renders the template and returns it as a httpresponse
    return render(request, 'forums/index.html', context)


# Shows a post based on the url
def detail(request, post_id):
    # Try to get the object from the model selected using the parameters specified, and if it fails return a 404 error.
    post = get_object_or_404(Post, pk=post_id)
    # Then render as normal
    return render(request, 'forums/detail.html', {'post': post})

# Handles the input from the form on the detail page
def reply(request, post_id):
    # get the post object the reply is going to be related to
    post = get_object_or_404(Post, pk=post_id)
    # check if the text input is empty or not
    if request.POST['reply']:
        # if it isnt empty create a reply object and redirect back to detail
        reply = Reply.objects.create(text=request.POST['reply'], post=post, reply_date=datetime.now())
        return HttpResponseRedirect(reverse('forums:detail', args=(post.id,)))
    else:
        # if its empty then render the template with an error message
        return render(request, 'forums/detail.html', {'post': post, 'error_message': 'You didnt enter anything',})

# Browse all the posts with 5 posts to a page
def browse(request, page_num=1):
    # Orders the posts by their post date and then only gets the ones belonging to the current page
    post_list = Post.objects.order_by('-post_date')[(page_num-1)*5:page_num*5]
    # Determine the max number of pages based on the number of posts. (the adding .5 is so that it always rounds the number up)
    page_max = round((Post.objects.count() / 5) + 0.5)
    # Renders the browser page
    return render(request, 'forums/browse.html', {'post_list': post_list, 'page_max': page_max, 'page_num': page_num})