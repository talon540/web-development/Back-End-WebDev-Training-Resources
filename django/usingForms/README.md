# Using Forms In Django

We are now going to make a couple adjustments to detail.html so that you can submit new replies.
To do this we add a couple of things. First, we add a spot for any error messages we may want to add later.
And then we add a html form that is directed to a url with the name of 'reply'. Of course we dont have a url by that name but we will make one in a second.
You might notice the csrf_token thing in our form as well, this is a django thing and you dont need to worry about it too much. Just understand that it is needed for any html form and is for security purposes.
```html
<h1>{{ post }}</h1>
<p>
    {{ post.text }}
</p>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<form action="{% url 'forums:reply' post.id %}" method="post">{% csrf_token %}
    <input type="text" name="reply">
    <input type="submit" value="reply">
</form>

{% for reply in post.reply_set.all %}
    <h5>{{ reply.reply_date }}</h5>
    <p>{{ reply.text }}</p>
{% endfor %}
```

Now we need to create the actual view for the reply form, so first we need to create a url.
Change your urls.py file to this:
```python
from django.urls import path

from . import views

app_name='forums'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:post_id>/', views.detail, name='detail'),
    path('<int:post_id>/reply/', views.reply, name='reply'),
    path('browse/', views.browse, name='browse'),
    path('browse/page<int:page_num>/', views.browse, name='browse'),
]
```

Now you can go ahead and add the reply view.
All of the inputs from the html form in the template are stored inside the http request.
We can access them by using request.POST[] and putting the name of the html input inside the brackets.
For example: request.POST['reply']
Also, you will see us using the reverse function in this view.
The reverse function uses namespacing to return the full url. This allows you to avoid hardcoding urls in your views.
We also create new objects in our models using the create function. We simply call it and include any fields we want to assign values to in it.
Note: if you ever change any values on an object you need to call the save function on it afterwards for your changes to have effect.
For example:

post.views += 1

post.save()

```python
from django.http import Http404, HttpResponseRedirect
from django.shortucts import get_object_or_404, render
from django.urls import reverse
from datetime import datetime

from .models import Post Reply

#...

# Handles the input from the form on the detail page
def reply(request, post_id):
    # get the post object the reply is going to be related to
    post = get_object_or_404(Post, pk=post_id)
    # check if the text input is empty or not
    if request.POST['reply']:
        # if it isnt empty create a reply object and redirect back to detail
        reply = Reply.objects.create(text=request.POST['reply'], post=post, reply_date=datetime.now())
        return HttpResponseRedirect(reverse('forums:detail', args=(post.id,)))
    else:
        # if its empty then render the template with an error message
        return render(request, 'forums/detail.html', {'post': post, 'error_message': 'You didnt enter anything',})
        
#...
```
It is important to always redirect after handling a form intead of rendering a template, because if you didnt it would resend the data whenever someone refreshed the page.

Now you are ready to move onto [automatic testing](../addingTests/README.md)