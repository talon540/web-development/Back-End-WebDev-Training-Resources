from django.urls import path

from . import views

app_name='forums'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:post_id>/', views.detail, name='detail'),
    path('<int:post_id>/reply/', views.reply, name='reply'),
    path('browse/', views.browse, name='browse'),
    path('browse/page<int:page_num>/', views.browse, name='browse'),
]