# Using Static Files With Your Templates

You might have wondered at some point during these lessons about how you are supposed use css, images, javascript or any other 'static' files with your templates.
Well it is actually pretty simple, it just needs a little bit of setup.

First we create a directory called static inside of the forums app. Then inside of that we create a forums directory, similar to how you structured the templates.
Inside of this you can now add whatever static files you want, for example we will add style.css

```css
body {
    background-color: lightblue;
}
```

Now in our templates we add this at the top:

```html
{% load static %}

<link rel="stylesheet" type="text/css" href="{% static 'forums/style.css' %}" />
```

And if you reload your page the background should now be blue. (note: you might have to restart your development server in order for it to work)
    ![blue background](../images/bluebackground.PNG)
    

That is all it really takes to get static files working, and all you really need for a development server.
Though i do recommend adding folders for css, images, or whatever else in there instead of just putting the files in it.

You are now ready to move on to [customizing the admin interface](../usingDjangoAdmin/README.md)