# Customizing the Admin Interface

Part of what makes django's admin interface so nice is how it is really easy to customize it however you want.
As you already know you can add whatever models you want to it, but you can also customize the form for submitting and viewing data, as well as even customizing the individual templates used for django admin.

For example if you wanted to change the order of the fields in the post model form you would do something like this:
```python
from django.contrib import admin

from .models import Post, Reply

class PostAdmin(admin.ModelAdmin):
    fields = ['title', 'post_date', 'views', 'text']

admin.site.register(Post, PostAdmin)
admin.site.register(Reply)
```
We simply added a class that is a subclass of modeladmin and gave it an instance variable that django recognizes and will use.
Then its as simple as adding it to the model when we register it.

And now the post edit form looks like this:
    ![post form](../images/postform.PNG)
    
Something annoying about the form as it is right now is that you can't see which replies relate to the current post, so lets fix that.
The way we fix it is by creating another class called choiceinline that extends django's stackedinline class. We can then reference this class in the postadmin class.
```python
from django.contrib import admin

from .models import Post, Reply

class replyInLine(admin.StackedInline):
    model = Reply
    extra = 1

class PostAdmin(admin.ModelAdmin):
    fields = ['title', 'post_date', 'views', 'text']
    inlines = [replyInLine]
    

admin.site.register(Post, PostAdmin)
admin.site.register(Reply)
```

After changing it, if you go to the admin form you should be able to see the change:
    ![admin post form relationship](../images/adminformrelation.PNG)

I've gone ahead and made a couple more changes here:
```python
from django.contrib import admin

from .models import Post, Reply

class replyInLine(admin.StackedInline):
    model = Reply
    extra = 1

class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['title', 'text']}),
        ('Date information', {'fields': ['post_date'], 'classes': ['collapse']}),
    ]
    inlines = [replyInLine]
    
    list_display = ('title', 'post_date', 'views')
    
class ReplyAdmin(admin.ModelAdmin):
    fields=['post', 'text', 'reply_date']
    list_display = ('post', 'reply_date')
    

admin.site.register(Post, PostAdmin)
admin.site.register(Reply, ReplyAdmin)
```
Lets go over these:
* fieldsets - seperates the page into a couple different categories. The first variable is the name of the category, the second is the fields in it, and after that is any optional arguments.
* list_display - this changes the way the posts and replies are listed when you are browsing them.
* replyadmin - added some changes to the reply form and list as well

And this is what they result in on the admin post form:
    ![some changes to the admin pots form](../images/somechanges.PNG)
    
And this is what the post and reply lists looks like:
    ![post lists](../images/postlist.PNG)
    ![reply list](../images/replylist.PNG)
    
Lets try actually customizing one of the admin templates.
To do this create a directory called admin instide of templates and copy admin/base_site.html from django/contrtib/admin/templates (The django files are located in the virtual env under lib/python3.4/site-packages/)

Change where it says _{{ site_header|default:_('Django adminsistratin') }}_ to something like this:
```html
{% block branding %}
<h1 id="site-name"><a href="{% url 'admin:index' %}">Henry ge is a thot</a></h1>
{% endblock %}
```
What we have just done is change the header at the top of any of the admin pages. For example, you can see it here:
    ![admin header](../images/adminheader.PNG)
    
You should now understand the general basics of customizing the admin interface, but there is of course a lot more you can do as well. Check out the [doucmentation](https://docs.djangoproject.com/en/2.0/ref/contrib/admin/) if you want to know more.

Here is a basic list of some things you might want to check out:
* Generic views
* Class based views
* Model Relationships
* View some ofthe other shortcut functions
* Django Authentication
* Django Form classes
* Logging
* Pagination

You are now finished with the django training, congrats, now go complete your last assignments.