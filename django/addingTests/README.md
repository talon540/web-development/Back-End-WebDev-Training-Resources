# Automated Testing

Automated tests are a set of tests created once by us that we can call later as we change our app to ensure that our app still works.
Although creating tests may seem tedious or boring at times, they are incredibly important.

Once you reach a certain point you won't always notice when your new changes to the code breaks something in a completely different part of the website, but tests could detect that.
Its best to think of tests as a way of preventing problems.

## Writing a basic test
Go ahead and open your tests.py file and add the following code.
```python
from django.test import TestCase
from forums.models import Post, Reply
from django.utils import timezone

class PostTest(TestCase):
    
    def test_post_creation(self):
        post = Post.objects.create(title='title', text='test text', post_date=timezone.now(), views=0)
        self.assertTrue(isinstance(post, Post))
        self.assertEqual(post.__str__(), post.title)
```

We start by creating a class called posttest which we are going to use to test the post model.
In this class we create functions that are run as tests. For a function to be considered a test its name must start with 'test'.
For this example we created a test to make sure the post model is able to properly create objects.
We start by creating a post object, and then we can use the assert functions to check somethings about it.
The asserttrue function checks if the isinstance function returns true, basically it checks if the post object is an instance of the Post class.
Then there is the assert equal function, which is pretty self explanatory.

Now that we have a test we can run it:
```shell
python manage.py test forums
```

Though you probably got an error saying permission denied. This is because I am an idiot and forgot a step when creating our postgreSQL user.
Go ahead and login back to the postgres user and open postgres.
Then do the following:
```shell
$ sudo su postgres
$ psql
# ALTER USER exampleuser CREATEDB;
```

Now if you try running your test again it should pass.
The reason we got that error for permission denied is because everytime you run these tests django creates a test database seperate from the actual database.
This way all your data can stay safe and you can enter your own test data safely.

Lets go ahead and add a test to check if the index page loads properly.
```python
from django.test import TestCase
from forums.models import Post, Reply
from django.utils import timezone
from django.urls import reverse

class PostTest(TestCase):
    
    def test_post_creation(self):
        post = Post.objects.create(title='title', text='test text', post_date=timezone.now(), views=0)
        self.assertTrue(isinstance(post, Post))
        self.assertEqual(post.__str__(), post.title)
        
class IndexTest(TestCase):
    
    def test_index_load(self):
        response = self.client.get(reverse('forums:index'))
        self.assertEqual(response.status_code, 200)
```

In this test we use the test client to test loading a webpage. The client is a really powerful tool for testing in django.
This allows us to do a lot of different tests, for example we could check the content of the page using response.content, or like we did in this example we can check the status code (code 200 just means that it loaded fine).
We could also use it to submit forms or check if authentication is working.

Now you know the basics of automated testing in django. Though I would highly recommend checking out the [documentation](https://docs.djangoproject.com/en/2.0/topics/testing/) and trying to add more tests to our example website.