# Django Training Resources

This directory contains a series of lessons that aim to establish a solid understanding of the basics of the django web framework.

Django is what will be used to actually build the website and bring all of it together.

If you ever get confused about something make sure to check the [documentation](https://docs.djangoproject.com/en/2.0/), because django has some beautiful and really helpful documentation.
Its also just a good idea to check it out evein if you aren't confused, because there is a lot more to learn that isn't covered in these lessons.

## Table of Contents
Each part of this tutorial will teach a new part of django and add something to the example website.

1. [Basic Views and Urls](basicViewsAndUrls/README.md)
2. [Models and Admin Intro](modelsAndIntroToDjangoAdmin/README.md)
3. [Using Templates](templatesAndNamespacingUrls/README.md)
4. [Using Forms](usingForms/README.md)
5. [Adding Tests](addingTests/README.md)
6. [Using Static Files](staticFiles/README.md)
7. [Using Django Admin](usingDjangoAdmin/README.md)

## Installation Procedure
You are able to install django on your own personal machine however because we will be using cloud9 to actually develope the website, that is what will be used for these lessons.

### Creating a Cloud9 Workspace
To begin you will have to create a cloud9 workspace to work in.

1. Go to [cloud9](https://c9.io) and login using the credentials given to you at the meeting
2. Click the 'create a new workspace' button
3. Fill out the required fields then choose the blank template (make sure not to select the django template, it is an outdated version, and its a pain to update it in cloud9)

    ![The workspace creation](images/cloud9workspace.PNG)

Now you should have a blank workspace ready to install django on

### Creating a Virtual Environment for Django
Now you have to create a virtual environment to run django in.

A virtual environment is an environment that isolates libraries, scripts, interpreters, and many more things.
This allows us to not have to install a lot of things throughout the entire system, it also allows us to run multiple versions of django on different virtualenvironments if we wanted to.
First, install the virtualenv tool which we will use to create the virtual environment.
```shell
sudo pip3 install virtualenv
```

Now that you have virtualenv installed you can actually create the environment using the virtualenv command.
All you need to do for the command is type virtualenv followed by the name of the environment, like this:
```shell
virtualenv django-example
```

To actually work in the virtual environment that you have created you must use the following command.
Replace django-example with whatever the name of your virtual environment is.
```shell
dr_kerbal:~/workspace $ source django-example/bin/activate
(django-example) dr_kerbal:~/workspace $ 
```

Anytime you want to exit the virtual environment you simply need to use the deactivate command.
```shell
(django-example) dr_kerbal:~/workspace $ deactivate
dr_kerbal:~/workspace $ 
```

Now you know how to create and use a virtual environment, and are finally ready to install and use django.

### Installing and Running Django
Now we can finally install and run django!

Installing django is now pretty straightforward, its just one command. (just make sure you are in your virtual environment first. You will know if the name of it is in parenthesis in the terminal)
```shell
pip install Django
```

You can verify that django is working if you run python by typing python then doiong the following. Afterwards you can quit with quit()
```python
>>> import django
>>> print(django.get_version())
2.07
```

Now django is installed and you are ready to use it.

## Getting Started
Now that django is installed you are ready to start working with it.

The next step is create a project to work in. Replace example-site with whatever you want your project to be called.
```shell
django-admin startproject examplesite
```

Now your file structure should something like this
```
.
├── django-example
└── examplesite
    ├── examplesite
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    └── manage.py
```
Now there are a bunch of different files that you probably are confused about, this is what they do:
* The outer most examplesite directory is the directory for the entire project, it isnt that important.
* manage.py is a command line utility that you will be using a lot, from running your development server to migrating your models.
* The inner examplesite directory is the python package used by your project.
* \_\_init\_\_.py is an empty file that simply lets python know the directory is a python package.
* settings.py is pretty self explanatory, it contains all the settings and configuration for your project.
* urls.py contains the url declarations for your website, and we will get into that in more depth later.
* wsgi.py is an entry-point for wsgi web servers, you dont need to worry about this too much.

And now it is finally time to actually run your website.
We will now use that command line utility from earlier, manage.py.
It is pretty easy to use it, we just need to make sure to use the ip and port that cloud9 has specified so that we can acess the website.
```shell
cd examplesite/
python manage.py runserver $IP:$PORT
```

Once it is running cloud9 should pop up with a link that you can go to the website.
    ![c9 giving the url](images/c9url.PNG)
    
Now the website is running, and you can view it! Except you have an error.
This is because we are using it through cloud9 which uses a url that django doesnt recognize.
To fix it open settings.py and look for this:
```python
ALLOWED_HOSTS = []
```

And add whatever the error told you to add to it.
```python
ALLOWED_HOSTS = ['django-example-site-dr-kerbal.c9users.io']
```

And now it should be finally working! Though now the next step is to get it working with postgreSQL
    ![The django working installation page](images/djangoworking.png)
    
## Using PostgreSQL with Django
The first thing we need to do to setup postgresql for django is create the database that it will use, as it already comes preinstalled on c9.
To do this you need to start posgresql, switch to the postgres user using the sudo su command, and then you can run postgreSQL.
```shell
sudo service postgresql start
sudo su postgres
psql
```

Now we create a user for django to use, simply replace exampleuser with whatever you want to call it and password with whatever you want your password to be (Note: you do need a password otherwise django complains and it wont work). 
```shell
# CREATE USER exampleuser WITH PASSWORD 'password';
```

Next, since we have a working user for django to use, we also need a database for it to work in. Again just replace exampledb with whatever your name of the database is, and exampleuser with your user's name.
```shell
# CREATE DATABASE exampledb OWNER exampleuser;
```

You can then exit out by doing \q, then exit to switch back to the normal user.

Now just install psycopg2 (a package python needs for communicating with postgres), and make sure you do this in your virtual environment
```python
pip install psycopg2
```

Finally, we just need to change a couple of things in settings.py.
Find this somewhere in the file:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
```
And replace it with this:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'exampledb',
        'USER': 'exampleuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}
```

Finally you can migrate all of your databases, we will talk about what this actually means later, but for now just make sure there aren't any errors.
```shell
python manage.py migrate
```
[Now you are finally able to move on to the other lessons!](basicViewsAndUrls/README.md)