from django.http import HttpResponse

def index(request):
    return HttpResponse('hello world!')
    
def detail(request, post_id):
    return HttpResponse('Post:' + str(post_id))

def browse(request, page_num=1):
    return HttpResponse('Page: ' + str(page_num))