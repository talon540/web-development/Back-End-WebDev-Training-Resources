# Basic Views and Urls

Note: Please go back and follow the installation and getting started procedures in the [main readme](../README.md) if you haven't already

Now before we properly begin, there is actually one more step to the setup.
And that step is creating an app. An app in django is simply any web app with its own data, views, and more.
The best way to think of it is, if you can take it and put it on another website, it should be its own app.
For example, on the old website the scouting service would be its own app, and the login service would be another app.
To actually tell django to make the app we use manage.py again.
```shell
python manage.py startapp forums
```
You can replace forums with whatever you want the name of your app to be, but for this tutorial we will be making a basic forums web app.

And now your file structure should look like this:
```
.
├── django-example
└── examplesite
    ├── examplesite
    │   ├── __init__.py
    │   ├── settings.py
    │   ├── urls.py
    │   └── wsgi.py
    ├── forums
    │   ├── migrations
    │   │   └── __init__.py
    │   ├── __init__.py
    │   ├── admin.py
    │   ├── apps.py
    │   ├── models.py
    │   ├── tests.py
    │   └── views.py
    └── manage.py
```
Lots more files now:
* The forums directory is the actual python package containing your app
* migrations/ contains all of your database migrations which we will talk about later
* admin.py is how we will interact with django's admin interface
* apps.py can be used to configure some settings for your app, but we wont ever need to use it.
* models.py contains all of your models, well get to those later
* tests.py is where you will put your tests for your app, again, well get to that later
* views.py is where you will put your views which is what we are about to use

To start with lets create a basic view then try routing a url to it.

First open views.py, this is what you should see:
```python
from django.shortcuts import render

# Create your views here.
```

Now lets start by replacing it with this:
```python
from django.http import HttpResponse

def index(request):
    return HttpResponse('hello world!')
```

Of couse, you probably don't know what a view is actually supposed to do so this probably doesn't mean much.
Basically, a view at its most basic functionality is given a http request based on the url and then it has to return a response.
Of course this can vary quite a bit, as later we will be using templates for html, be returning 404 errors, or many more things.
Just understand that we set up a url that tells django which view to go to, that view gets a http request, and we return a response of some sort.

Now lets create a url for this view so that we can see it on our website.
First lets go to the urls.py file in the same folder with settings.py and wsgi.py.
Inside you should see this:
```python
from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('admin/', admin.site.urls),
]
```
For now, ignore the admin part and change it so it looks like this
```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('forums/', include('forums.urls')),
    path('admin/', admin.site.urls),
]
```
Now create a file in your app directory and name it urls.py then open it and add this to it:
```python
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
]
```
The path has 3 parts to it, the first being the url, the second being the view it goes to, and the third is the name it will be referenced by in your code later.
Now if you go to example.com/forums/ (replace example.com with the c9 url) you should see a blank page with hello world.

The reason this works is because in django it takes whatever is after the base url and reads it bit by bit matching it with whats in your url files
For example if you had a url of example.com/forums/example/url/
It would start by stripping it down to 'forums/example/url/'
Now it would go the starting urls.py file in the same folder with your settings.py and wsgi.py.
Here it compares the string to the paths in urlpatterns.
It matches it with the 'forums/' path which tells it to go the the urls.py file in the forums app.
However there it cant match anything with 'example/url/' so you would get a 404.

Though if you do go to that url, since we have debugging enabled it will show you the urls it tried to check.
    ![django url checking](../images/urlchecking.PNG)

Now we can try adding some more views, which can include arguments in the url.
To start lets add the urls for the views:
```python
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:post_id>/', views.detail, name='detail'),
    path('browse/', views.browse, name='browse'),
    path('browse/page<int:page_num>/', views.browse, name='browse'),
]
```
Now you might notice the parts of the urls such as '\<int:post_id\>', these are spots that allow for arguments to passed to the views.
This one for example takes an in, such as example.com/forums/540/, and passes that int, 540, to the detail view as post_id.
Now you actually use these arguments in views like this:
```python
from django.http import HttpResponse

def index(request):
    return HttpResponse('hello world!')
    
def detail(request, post_id):
    return HttpResponse('Post:' + str(post_id))

def browse(request, page_num=1):
    return HttpResponse('Page: ' + str(page_num))
```
To add an argument, you simply add the name of it after the request argument.
In browse you may notice it also adds =1 to it, this simply means it defaults to 1 if no argument is given.

Now you understand the basics of working with views and urls, of course there is a lot more, but first it is time to [learn about models.](../modelsAndIntroToDjangoAdmin/README.md)