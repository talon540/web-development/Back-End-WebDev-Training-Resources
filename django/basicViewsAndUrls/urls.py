from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:post_id>/', views.detail, name='detail'),
    path('browse/', views.browse, name='browse'),
    path('browse/page<int:page_num>/', views.browse, name='browse'),
]