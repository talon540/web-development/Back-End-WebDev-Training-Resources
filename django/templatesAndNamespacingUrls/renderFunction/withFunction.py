from django.shortcuts import render

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # The context is what will be passed to the template. The strings on the left are the names of the variables in the template, and the variables on the right are what they are asigning the template variables to.
    context = {
        'recent_post_list': recent_post_list,
        'popular_post_list': popular_post_list,
    }
    # renders the template and returns it as a httpresponse
    return render(request, 'forums/index.html', context)