from django.http import HttpResponse
from django.template import loader

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # Gets the template you are wanting to render
    template = loader.get_template('forums/index.html')
    # The context is what will be passed to the template. The strings on the left are the names of the variables in the template, and the variables on the right are what they are asigning the template variables to.
    context = {
        'recent_post_list': recent_post_list,
        'popular_post_list': popular_post_list,
    }
    # Now it finally returns the template rendered and as a httpresponse
    return HttpResponse(template.render(context, request))