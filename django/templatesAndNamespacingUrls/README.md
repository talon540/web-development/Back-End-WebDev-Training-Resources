# Using Django Templates

Now as you are probably aware, the way we have been making our views we are simply giving a string as an httpresponse.
This of course is really limiting and inefficient. Luckily Django provides a wonderful system for using html combined with some special syntax that can be used to handle data.

To begin making templates you want to start by making a templates directory in your app. Then make another directory inside that called forums, or whatever the name of your app is.
Then finally you can create a file, such as index.html.
Now wiith the way django loads templates we are able to refer to the template as forums/index.html
The reason we didnt just put the index.html template directly inside the templates folder is because if another app had a template called index.html then django would get confused and would not know which one to use.

In index.html add the following code:
```python
{% if recent_post_list and popular_post_list %}
    <h1>Popular Posts</h1>
        <ul>
        {% for post in popular_post_list %}
            <li><a href="/{{ post.id }}">{{ post }}</a></li>
        {% endfor %}
        </ul>
    <h1>Recent Posts</h1>
        <ul>
        {% for post in recent_post_list %}
            <li><a href="/{{ post.id }}">{{ post }}</a></li>
        {% endfor %}
        </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
```
In the template syntax you can use either {% %} or {{ }} to use python code in your html code.
The difference is that you use {% %} if your code is actually doing something like an if statement or for loop and you use {{ }} if you are simply getting a value or something similar.

Now we have to render the template in our view.

To this we use this code:
```python
from django.http import HttpResponse
from django.template import loader

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # Gets the template you are wanting to render
    template = loader.get_template('forums/index.html')
    # The context is what will be passed to the template. The strings on the left are the names of the variables in the template, and the variables on the right are what they are asigning the template variables to.
    context = {
        'recent_post_list': recent_post_list,
        'popular_post_list': popular_post_list,
    }
    # Now it finally returns the template rendered and as a httpresponse
    return HttpResponse(template.render(context, request))
```

Though django also provides a nice shortcut for all of this called render()
```python
from django.shortcuts import render

from .models import Post, Reply

#Show the latest 5 posts, and the 5 most viewed posts
def index(request):
    # Gets the latest 5 posts
    recent_post_list = Post.objects.order_by('-post_date')[:5]
    # Gets the top 5 posts
    popular_post_list = Post.objects.order_by('-views')[:5]
    # The context is what will be passed to the template. The strings on the left are the names of the variables in the template, and the variables on the right are what they are asigning the template variables to.
    context = {
        'recent_post_list': recent_post_list,
        'popular_post_list': popular_post_list,
    }
    # renders the template and returns it as a httpresponse
    return render(request, 'forums/index.html', context)
```

You can find the detail.html and brose.html templates in the templates directory within the same directory as this readme.

Now we are going to try raising a 404 error for when we attempt to access a detail view of a post that doesnt exist
```python
# Shows a post based on the url
def detail(request, post_id):
    try:
        # Attempt to get a post based on its id. If get cant find anything it should raise a doesnotexist error
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        # If there is a doesnotexist error than we simply raise a 404 instead of giving a httpresponse
        raise Http404('Post does not exist')
    # Otherwise render the page normally
    return render(request, 'forums/detail.html', {'post': post})
```
We simply use a try and except statement in python.
Though django of course provides a much easier way of doing this. Its called the get_object_or_404() function.
```python
from django.shortcuts import get_object_or_404, render

# Shows a post based on the url
def detail(request, post_id):
    # Try to get the object from the model selected using the parameters specified, and if it fails return a 404 error.
    post = get_object_or_404(Post, pk=post_id)
    # Then render as normal
    return render(request, 'forums/detail.html', {'post': post})
```

You may have noticed that for the urls in our templates we have been hardcoding the values. This is bad and goes against a lot of what django stands for, therefore the proper way to do it is through the url tag and namespacing.
First we must add app_name to our urls.py file like this:
```python
from django.urls import path

from . import views

app_name='forums'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:post_id>/', views.detail, name='detail'),
    path('browse/', views.browse, name='browse'),
    path('browse/page<int:page_num>/', views.browse, name='browse'),
]
```

Now in our template we can use the url tag like this:
```python
{% url 'forums:detail' post.id %}
```
The url tag is structured with the url tag first, followed by the url to go to, and then any parameters for the url.
We reference the detail url by using the app name followed by the name of the url.

Check the index.html file in the templates directory to see how this is used for html links.

And now we are ready to move onto [using forms in django](../usingForms/README.md)