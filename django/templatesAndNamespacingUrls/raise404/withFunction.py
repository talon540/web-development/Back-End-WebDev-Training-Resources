from django.shortcuts import get_object_or_404, render

from .models import Post

#...

# Shows a post based on the url
def detail(request, post_id):
    # Try to get the object from the model selected using the parameters specified, and if it fails return a 404 error.
    post = get_object_or_404(Post, pk=post_id)
    # Then render as normal
    return render(request, 'forums/detail.html', {'post': post})
    
#...