from django.http import Http404
from django.shortcuts import render

from .models import Post

#...

# Shows a post based on the url
def detail(request, post_id):
    try:
        # Attempt to get a post based on its id. If get cant find anything it should raise a doesnotexist error
        post = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        # If there is a doesnotexist error than we simply raise a 404 instead of giving a httpresponse
        raise Http404('Post does not exist')
    # Otherwise render the page normally
    return render(request, 'forums/detail.html', {'post': post})

#...